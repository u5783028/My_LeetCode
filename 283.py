class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        if nums:
            index = 0
            zero = 0
            while index <len(nums):
                if nums[index] == 0:
                    zero+=1
                    nums.pop(index)
                else:
                    index +=1

            nums.extend([0]*zero)