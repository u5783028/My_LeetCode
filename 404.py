# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None




class Solution(object):
    def sumOfLeftLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        sum = 0
        if root.right !=None :
            return left_most(root)
        elif root.left!= None:
            return left_most(root)+self.sumOfLeftLeaves(root.right)
