import bisect

class Solution(object):
    def lengthOfLIS(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """


        # first thought: naive dp approach
        # really bad running time : 1000ms+  O(n^2)
        # res = [1]*len(nums)
        # for i in range(1,len(nums)):
        #     for j in range(0,i):
        #         if nums[i]>nums[j] and res[j]+1>res[i]:
        #             res[i]= res[j]+1
        # return max(res)



        #imporved version running time: 50ms  O(nlogn)
        #res[i]  represent integer which the longest substring  with length i+1 ends with
        #we always updated res when we reading new element from the list
        
        res = []
        for num in nums:
            index = bisect.bisect_left(res,num)
            if index == len(res):
                res.append(num)
            else:
                res[index]=num
        return len(res)