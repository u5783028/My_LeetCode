class Solution(object):
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """

        matrix = [[0 for i in range(n)] for j in range(n)]
        r_start,r_end = 0,n-1
        c_start,c_end = 0,n-1

        direction,index = 0,1

        while r_start<=r_end and c_start<=c_end:
            if direction % 4 == 0:
                for i in range(c_start,c_end+1):
                    matrix[r_start][i]=index
                    index+=1
                r_start+=1
            elif direction % 4 == 1:
                for i in range(r_start,r_end+1):
                    matrix[i][c_end]=index
                    index+=1
                c_end-=1
            elif direction % 4 == 2:
                for i in range(c_end,c_start-1,-1):
                    matrix[r_end][i]=index
                    index+=1
                r_end-=1
            else:
                for i in range(r_end,r_start-1,-1):
                    matrix[i][c_start]=index
                    index+=1
                c_start+=1

            direction+=1
        return matrix


#test
sol = Solution()
m = sol.generateMatrix(0)
for i in m:
    print(i)