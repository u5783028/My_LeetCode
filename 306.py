    class Solution(object):
        def isAdditiveNumber(self, num):
            """
            :type num: str
            :rtype: bool
            """

            l = len(num)
            if  l <3:
                return False

            for i in range(1,l//3+1):

                if len(num[:i]) > 1 and num[:0][0] == "0":
                    return False

                for j in range(i+1,(l-i)/2+i+1):
                    first  = num[:i]
                    second  = num[i:j]
                    others=num[j:]

                    if len(second)>1 and second[0]=="0":
                        continue

                    while others:
                        c = str(int(first)+int(second))
                        if c ==others:
                            return True
                        elif others.startswith(c):
                            first = second
                            second = c
                            others =others[len(c):]
                        else:
                            break

            return False


"""
itertools.combinations

combinations(iterable, r) --> combinations object
 |
 |  Return successive r-length combinations of elements in the iterable.
 |
 |  combinations(range(4), 3) --> (0,1,2), (0,1,3), (0,2,3), (1,2,3)
"""


"""
startswith(...)
    S.startswith(prefix[, start[, end]]) -> bool

    Return True if S starts with the specified prefix, False otherwise.
    With optional start, test S beginning at that position.
    With optional end, stop comparing S at that position.
    prefix can also be a tuple of strings to try.
"""