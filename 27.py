class Solution(object):
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """

        length = len(nums)
        for i in range(length-1,-1,-1):
            if nums[i] == val:
                nums.pop(i)
        return nums

sol = Solution()
nums = [3,2,2,3]
print(sol.removeElement(nums,3))