from itertools import *

class Solution(object):
    def readBinaryWatch(self, num):
        """
        :type num: int
        :rtype: List[str]
        """


        res = []
        time = [100,200,400,800,1,2,4,8,16,32]
        possible = tuple(combinations(time,num))
        for i in possible:
            t = sum(i)
            if t<1200 and t%100<60:
                if t%100 == 0:
                    res.append(str(t/100)+":00")
                elif t%100<10:
                    res.append(str(t//100)+":0"+str(t%100))
                else:
                    res.append(str(t//100)+":"+str(t%100))
        return res