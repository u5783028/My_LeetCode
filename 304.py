class NumMatrix(object):
    def __init__(self, matrix):
        """
        initialize your data structure here.
        :type matrix: List[List[int]]
        """
        self.matrix = matrix
        self.sum = self.getDp()

    def sumRegion(self, row1, col1, row2, col2):
        """
        sum of elements matrix[(row1,col1)..(row2,col2)], inclusive.
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """

        return  self.sum[row2+1][col2+1]+self.sum[row1][col1]-self.sum[row1][col2+1]-self.sum[row2+1][col1]

    def getDp(self):
        if len(self.matrix) == 0:
            return []
        rows, cols = len(self.matrix),len(self.matrix[0])
        sums = [[0 for i in range(cols+1)] for j in range(rows+1)]
        for i in range(1,rows+1):
            for j in range(1,cols+1):
                sums[i][j] = sums[i-1][j]+sums[i][j-1]-sums[i-1][j-1] +self.matrix[i-1][j-1]
        return sums



# Your NumMatrix object will be instantiated and called as such:
matrix = [
    [3, 0, 1, 4, 2],
    [5, 6, 3, 2, 1],
    [1, 2, 0, 1, 5],
    [4, 1, 0, 1, 7],
    [1, 0, 3, 0, 5]
]
numMatrix = NumMatrix(matrix)
print(numMatrix.sumRegion(2,1,4,3))
print(numMatrix.sumRegion(1,1,2,2))
print(numMatrix.sumRegion(1,2,2,4))

# matrix = []
# numMatrix = NumMatrix(matrix)


