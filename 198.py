class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if len(nums) ==0:
            return 0
        elif len(nums) <=2:
            return max(nums)
        else:
            max_rob = [nums[0],nums[1]]
            for i in range(2,len(nums)):
                max_rob.append(max(max_rob[j] for j in range(0,i-1))+nums[i])
            return max(max_rob)

# sol = Solution()
# nums = [9,10,7,3,9,21,23,3,6,8,42]
# print(sol.rob(nums))
#answer should be 96