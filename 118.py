class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """

        res = []
        if numRows>0:
            for i in range(numRows):
                level = [1]*(i+1)
                for j in range(1,i):
                    level[j]=res[i-1][j-1]+res[i-1][j]
                res.append(level)
        return res