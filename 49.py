class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        # hashtale {(a, b, c): ["abc", "bca"]}
        #
        map = {}
        for  str in strs:
            ls = list(str)
            lst = tuple(ls.sort())
            if map.get(lst):
                map.get(lst).append(str)
            else:
                map[lst] = [str]
        return map.values()