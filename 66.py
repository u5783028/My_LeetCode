class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        step = 1
        for i in range(len(digits)-1,-1,-1):
            if digits[i] + step ==10:
                digits[i]= 0
                step =1
            else:
                digits[i]+= step
                step =0
        if step == 1:
            digits.insert(0,1)
        return digits
