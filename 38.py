class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        if n==1:
            return "1"
        else:
            pre=self.countAndSay(n-1)
            current_num = pre[0]
            result = []
            count=0
            for char in pre:
                if char == current_num:
                    count+=1
                else:
                    result.append(count)
                    result.append(current_num)
                    current_num = char
                    count=1
            result.append(count)
            result.append(current_num)
            return ''.join(str(e) for e in result)