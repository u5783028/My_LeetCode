# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def removeElements(self, head, val):
        """
        :type head: ListNode
        :type val: int
        :rtype: ListNode
        """
        if head == None:
            return head
        if head.val == val:
            while head != None and head.val==val:
                head = head.next
        if head == None:
            return []
        current = head
        while current.next!= None:
            if current.next.val ==val:
                next = current.next
                while next !=None and next.val == val :
                    next = next.next
                current.next = next
            else:
                current = current.next
        return head