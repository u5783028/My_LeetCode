class Solution(object):
    def intersect(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        dic1,dic2 = {},{}
        for i in nums1:
            if i in dic1:
                dic1[i]+=1
            else:
                dic1[i]=1
        for i in nums2:
            if i in dic2:
                dic2[i]+=1
            else:
                dic2[i]=1
        res = []
        for i in set(nums1)&set(nums2):
            res.extend(min(dic1[i],dic2[i])*[i])
        return res