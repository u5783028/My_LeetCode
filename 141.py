# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def hasCycle(self, head):
        """
        :type head: ListNode
        :rtype: bool
        """

        slow ,fast = head,head
        while fast.next!=None:
            slow = slow.next
            fast = fast.next.next
            if fast ==None:
                return False
            if slow is fast :
                return True
        return False

    # try use error handling to reduce the running time
    # def hasCycle(self, head):
    #     try:
    #         slow = head
    #         fast = head.next
    #         while slow is not fast:
    #             slow = slow.next
    #             fast = fast.next.next
    #         return True
    #     except:
    #         return False