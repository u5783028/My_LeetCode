# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def preorderTraversal(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        node ,result = [], []
        node.append(root)

        while node:
            last =node.pop()
            if last.right != None: node.append(last.right)
            if last.left != None: node.append(last.left)
            result.append(last.val)
        return result

