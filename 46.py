import itertools

class Solution(object):
    def permute(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        # return [list(i) for i in itertools.permutations(nums)]

        res =[]
        if len(nums) <=1:
            return [nums]
        else:
            for i in range(len(nums)):
                r = self.permute(nums[:i]+nums[i+1:])
                for j in r:
                    j.insert(0,nums[i])
                res.extend(r)
        return res
