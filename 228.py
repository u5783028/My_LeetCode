class Solution(object):
    def summaryRanges(self, nums):
        """
        :type nums: List[int]
        :rtype: List[str]
        """
        if len(nums)==0:
            return []
        elif len(nums) ==1 :
            return [str(nums[0])]
        else:
            start = 0
            result = []

        for i in range(1,len(nums)):
            if nums[i]-nums[i-1] != 1:
                if start == i-1:
                    result.append(str(nums[start]))
                else:
                    result.append(str(nums[start])+"->"+str(nums[i-1]))
                start = i
        if nums[i]-nums[i-1]==1:
            result.append(str(nums[start])+"->"+str(nums[i]))
        else:
            result.append(str(nums[i]))

        return result