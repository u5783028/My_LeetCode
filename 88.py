class Solution(object):
    def merge(self, nums1, m, nums2, n):
        while m > 0 and n > 0:
            if nums1[m - 1] > nums2[n - 1]:
                nums1[m + n - 1] = nums1[m - 1]
                m -= 1
            else:
                nums1[m + n - 1] = nums2[n - 1]
                n -= 1
        nums1[:n] = nums2[:n]

sol = Solution()
a= [1,3,5,0,0,0,0,0,0,0]
sol.merge(a,3,[0,1,2,3,4,5,6],7)
print(a)