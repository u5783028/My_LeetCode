def pig_latin(str):
    vowels = 'aeiou'
    for i in range(0,len(str)):
        if str[i].lower() in vowels:
            break
    if i == 0 :
        new = str+"yay"
    else:
        new = str[i:len(str)]+str[0:i]+ "ay"

    return new

print(pig_latin("dOg"))
print(pig_latin("scratch"))
print(pig_latin("is"))
print(pig_latin("apple"))
print(pig_latin("fly"))