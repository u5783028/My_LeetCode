class Solution(object):
    def findNthDigit(self, n):
        """
        :type n: int
        :rtype: int
        """
        len,sum= 1,0
        if n <10:
            return n
        while n>sum+len*9*10**(len-1):
            sum+=len*9*10**(len-1)
            len+=1
        num = (n-1-sum)//len+10**(len-1)
        reminder = (n-sum)%len
        return int(str(num)[reminder-1])

# sol = Solution()
# print(sol.findNthDigit(11))
# for i in range(1,20):
#     print(sol.findNthDigit(i))