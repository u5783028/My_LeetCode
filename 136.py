class Solution(object):
    def longestConsecutive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """

        nums = set(nums)
        best = 0
        for num in nums:
            length=1
            n=num
            while n-1 in nums:
                length+=1
                nums.discard()
                n-=1
            while num+1 in nums:
                length+=1
                nums.remove(num+1)
                num+=1
            best = max(best,length)
        return best
