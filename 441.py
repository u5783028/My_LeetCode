from math import *

class Solution(object):
    def arrangeCoins(self, n):
        """
        :type n: int
        :rtype: int
        """

        return int((sqrt(8*n+1)-1)/2)

        # naive method to acculumate the sum until reaching n
        # can be improver by using binary search
        # row = 0
        # while row*(row+1)/2<n:
        #     row+=1
        # return row
        