from collections import Counter
class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: int
        """

        #slower solution 79ms
        a  = Counter(s)
        res,odd  = 0,0
        for i in a.keys():
            if a[i]%2 == 0:
                res+=a[i]
            else:
                odd =1
                res+=a[i]//2

        return res+odd
        # fastest solution
        # 39ms beat 96.99%
        # a = set ()
        # count = 0
        # for i in s:
        #     if i in a:
        #         a.remove(i)
        #         count+=2
        #     else:
        #         a.add(i)
        # if len(a)!=0 or count== 0 :
        #     return count+1
        # else:
        #     return count