class Solution(object):
    def minimumTotal(self, triangle):
        """
        :type triangle: List[List[int]]
        :rtype: int
        """
        if not triangle:
            return []
        res= triangle
        for i in range(1,len(triangle)):
            res[i][0]+=res[i-1][0]
            res[i][i]+=res[i-1][i-1]
            for j in range(1,i):
                res[i][j]+= min(res[i-1][j-1],res[i-1][j])
        return min(res[-1])

sol = Solution()
print(sol.minimumTotal([[2],[3,4],[6,5,7],[4,1,8,3]]))