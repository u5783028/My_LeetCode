class Solution(object):
    def addBinary(self, a, b):
        """
        :type a: str
        :type b: str
        :rtype: str
        """
        sum = int(a)+int(b)
        if sum == 0:
            return "0"
        step =0
        result = []
        while sum>0:
            reminder = (sum+step)%10
            sum = sum//10
            if reminder>=2:
                step = 1
            else:
                step = 0
            result.insert(0,str(reminder%2))
        if step ==1:
            result.append("1")
        return "".join( j for j in result)




sol = Solution()
print(sol.addBinary("1","11"))
