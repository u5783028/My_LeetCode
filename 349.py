class Solution(object):
    def intersection(self, nums1, nums2):
        """
        :type nums1: List[int]
        :type nums2: List[int]
        :rtype: List[int]
        """
        return list(set(nums1).intersection(set(nums2)))


sol = Solution()
nums1 = [1,2,2,3,4,5,6,7]
nums2= [2,4,6,9,10]
print(sol.intersection(nums1,nums2))
