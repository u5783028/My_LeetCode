# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    # see https://en.wikipedia.org/wiki/Cycle_detection for detail explanation
    def detectCycle(self, head):
        """
        :type head: ListNode
        :rtype: ListNode
        """
        slow,fast = head,head
        while fast and fast.next:
            slow = slow.next
            fast = fast.next.next
            if fast is slow:
                #we found a cycle ,then we need to work out the first element 
                return self.getStart(head,slow)
        return False

    def getStart(self,head,slow):
        while not (head is slow):
            head = head.next
            slow = slow.next
        return head