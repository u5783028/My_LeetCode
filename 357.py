class Solution(object):
    def countNumbersWithUniqueDigits(self, n):
        """
        :type n: int
        :rtype: int
        """
        if n ==0:
            return 0
        else:
            sum = 1
        for i in range(1,n+1):
            start,step = 9, 9
            for j in range(i-1):
                start*= step
                step-=1
            sum+=start
        return sum

    def fac(self,n):
        if n ==1:
            return 1
        else:
            return self.fac(n-1)*n