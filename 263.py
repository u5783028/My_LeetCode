class Solution(object):
    def isUgly(self, num):
        """
        :type num: int
        :rtype: bool
        """
        if num==0:
            return False
        ugly = [2,3,5]
        for i in ugly:
            while num%i==0:
                num/=i
        if num!=1:
            return False
        else:
            return True