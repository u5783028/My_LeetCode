class Solution(object):
    def convertToTitle(self, n):
        """
        :type n: int
        :rtype: str
        """
        result = ''
        while n>26:
            result += chr(ord("A")+n%26-2)
            n = n//26
        result += chr(ord("A")+n%26-1)
        return result[::-1]