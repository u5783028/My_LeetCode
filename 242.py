class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        dic = {}
        for i in s:
            if not i in dic:
                dic[i]=1
            else:
                dic[i]+=1
        for j in t:
            if not j in dic:
                return False
            elif dic[j]==1:
                dic.pop(j)
            else:
                dic[j]-=1
        if len(dic)!=0:
            return False
        else:
            return True
        ## 1-line python
        # return sorted(s)==sorted(t)

