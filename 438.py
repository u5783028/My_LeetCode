class Solution(object):
    def findAnagrams(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: List[int]
        """

        start ,end = 1,len(s)-len(p)
        target,temp = {},{}
        #init target dictionary
        for j in p:
            if j in target:
                target[j]+=1
            else:
                target[j]=1
        #init dictionary for the first substring
        for j in s[:len(p)]:
            if j in temp:
                temp[j]+=1
            else:
                temp[j]=1

        if target == temp:
            res = [0]
        else:
            res = []

        while start <= end:
            temp[s[start-1]]-=1
            if temp[s[start-1]]==0:
                temp.pop(s[start-1])
            if s[start+ len(p)-1] in temp:
                temp[s[start+len(p)-1]]+=1
            else:
                temp[s[start+len(p)-1]]=1

            if temp == target:
                res.append(start)
            start+=1
        return res











        # l1,l2 = len(s),len(p)
        # res = []
        # check = [1]*(l1-l2+1)
        # vocab = set(p)
        # for i in range(l1-l2+1):
        #     if s[i] not in vocab:
        #         for j in range(l2):
        #             if i-j>=0:
        #                 check[i-j]=0
        #
        # for i in range(l1-l2+1):
        #     if check[i]==1:
        #         if sorted(s[i:i+l2])==sorted(p):
        #             res.append(i)
        # return res

