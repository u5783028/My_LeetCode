class Solution(object):
    def wordPattern(self, pattern, str):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        words = str.split()
        if len(words)!=len(pattern):
            return False
        dic = {}
        for i in range(len(pattern)):
            if pattern[i] in dic:
                if dic[pattern[i]]!=words[i]:
                    return False
            else:
                dic[pattern[i]]=words[i]
                if len(set(dic.values()))!=len(dic.values()):
                    return False
            print(dic)

        return True
