class Solution(object):
    def spiralOrder(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: List[int]
        """

        if matrix:
            r_start,c_start =0,0
            r_end , c_end = len(matrix)-1,len(matrix[0])-1

            n = 0
            res = []
            while r_start<=r_end and  c_start<=c_end:

                if n%4 == 0:
                    res.extend(matrix[r_start][c_start:c_end+1])
                    r_start+=1
                elif n%4 ==1 :
                    for i in matrix [r_start:r_end+1]:
                        res.append(i[c_end])
                    c_end-=1
                elif n%4 == 2:

                    res.extend(matrix[r_end][c_start:c_end+1][::-1])
                    r_end-=1
                else:
                    for i in range(r_end,r_start-1,-1):
                        res.append(matrix[i][c_start])
                    c_start+=1
                n+=1
            return res
        else:
            return []



sol = Solution()
matrix =  [[1,2,3,4],[5,6,7,8],[9,10,11,12],[13,14,15,16]]
print(sol.spiralOrder(matrix))